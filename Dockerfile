FROM openjdk:11-jre-slim
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} guestbook.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/guestbook.jar"]
